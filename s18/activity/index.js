console.log("Hello World!");

/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/
	// Addition
	function addNum(num1, num2){
		let sumResult = num1 + num2;
		console.log("Displayed sum of " + num1 + " and " + num2);
		console.log(sumResult);
	}
	addNum(5,15);

	// Subtraction
	function subNum(num1, num2){
		let subResult = num1 - num2;
		console.log("Displayed difference of " + num1 + " and " + num2);
		console.log(subResult);
	}
	subNum(20,5);

	// Multiplcation
	function multiplyNum(num1, num2){
		console.log("The product of " + num1 + " and " + num2);
		return num1 * num2;
	}
	let product = multiplyNum(50,10);
	console.log(product);


	// Division
	function divideNum(num1, num2){
		console.log("The quotient of " + num1 + " and " + num2);
		return num1 / num2;
	}
	let quotient = divideNum(50,10);
	console.log(quotient);

	// Area of Circle
	function getCircleArea(num1){
		console.log("The result of getting the area of a circle with " + num1 + " radius:")
		const pi = 3.1416;
		return pi * (num1 ** 2);
	}
	let circleArea = getCircleArea(15);
	console.log(circleArea);

	// Average
	function getAverage(num1, num2, num3, num4){
		console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", " + "and " + num4)
		return (num1 + num2 + num3 + num4) / 4;
	}
	let averageVar = getAverage(20, 40, 60, 80);
	console.log(averageVar);

	// Check if passed
	function checkIfPassed(score, total){
		console.log("Is " + score + "/" + total +" a passing score?");
		let isPassed = ((score/total)*100) >= 76;
		return isPassed
	}
		let isPassingScore = checkIfPassed(38,50);
		console.log(isPassingScore);

//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
