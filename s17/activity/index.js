console.log("Hello World!");

/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:
	
	function printUserInfo() {
		let fullName = prompt("Enter your Full Name:");
		console.log("Hello, I'm " + fullName + ".");

		let age = prompt("Enter your Age:");
		console.log("I am " + age + " years old.");

		let location = prompt("Enter your Location:")
		console.log("I live in " + location);

		let catName = prompt("Enter your Cat's Name:");
		console.log("I have a cat named " + catName);

		let dogName = prompt("Enter your Dog's name:");
		console.log("I have a dog named " + dogName);
	}

	printUserInfo();

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

	function printFiveBands() {
		let nameFirstBand = prompt("Enter First Band:");
		console.log(nameFirstBand);

		let nameSecondBand = prompt("Enter Second Band:");
		console.log(nameSecondBand);

		let nameThirdBand = prompt("Enter Third Band:");
		console.log(nameThirdBand);

		let nameFourthBand = prompt("Enter Fourth Band:");
		console.log(nameFourthBand);

		let nameFifthBand = prompt("Enter Fifth Band:");
		console.log(nameFifthBand);

	}

	printFiveBands();

/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

	function printFiveMovies() {
		let nameFirstMovie = prompt("Enter First Movie:");
		console.log(nameFirstMovie);

		let nameSecondMovie = prompt("Enter Second Movie:");
		console.log(nameSecondMovie);

		let nameThirdMovie = prompt("Enter Third Movie:");
		console.log(nameThirdMovie);

		let nameFourthMovie = prompt("Enter Fourth Movie:");
		console.log(nameFourthMovie);

		let nameFifthMovie = prompt("Enter Fifth Movie:");
		console.log(nameFifthMovie);
	}

	printFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

/*console.log(friend1);
console.log(friend2);*/



//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}
