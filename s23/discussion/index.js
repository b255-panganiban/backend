console.log("Hello World!");

// [SECTION] Objects
/*
	- An object is a data type that is used to represent real world objects
	- It is a collection of related data and/or functionalities
	- In JavaScript, most core JavaScript features like strings and arrays are objects (String are a collection of characters and arrays are a collection of data)
	- Information stored in objects are represented in a "key:value" pair
	- A "key" is also mostly referred to as a "property" of an object
	- Different data types may be stored in an objects property creating complex data structures
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
};
console.log("Result from creating objects using initializers/literl notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);
console.log(cellphone.manufactureDate);

// Creating objects using a constructor function

/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurance of any object which emphasizes on the distinct/unique identity of it
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructors function paremeters
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

// The "new" operator creates an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructors");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using object constructors");
console.log(myLaptop);

// Creating empty objects
let computer = {}
let myComputer = new Object()

// [SECTION] Accessing Object Properties

let array = [laptop, myLaptop];

// May be confused for accessing array indexes
console.log(array[0]['name']);
// This tells us that array[0] is an object by using the dot notation
console.log(array[0].name);

// [SECTION] Initiatlizing/Adding/Deleting/Reassigning Object Properties
/*
	- Like any other variable in JavaScript, objects may have their properties initialized/added after object was created/declared
	- This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};

// Initializing/Adding object properties using dot notation
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);
console.log(car.name);

// Initializing/Adding object properties using square bracket notation
/*
	- While using the square bracket this will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accessed using the square bracket notation
*/
car["manufacture date"] = 2019;
console.log(car["manufacture date"]);
console.log(car["Manufacture Date"]);
console.log(car.manufactureDate);
console.log("Result from adding properties using square bracket notation");
console.log(car);

// Deleting object properties
delete car["manufacture date"];
console.log("Result from deleting properties")
console.log(car);

// Reassigning object properties
car.name = "Dodge Charge R/T";
console.log("Result from reassigning properties");
console.log(car);

// [SECTION] Object Methods
/*
	- A method is a function which is a property of an object
	- They are also functions and one of the key differences they have is that methods are functions related to a specific object
	- Mthods are useful for creating object specific functions which are used to perform tasks on them
*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
}

console.log(person);
console.log("Result from object methods:");
person.talk();

// Adding Methods to Objects
person.walk = function(){
	console.log(this.name + " Walked 25 steps forward")
};
person.walk();

// Mini-Activity
// Add a method to the person object
// That method should be able to console.log a hobby being done by John
// use "this" keyword to access the name John

person.hobby = function(){
	console.log(this.name + " played basketball")
};
person.hobby();

// Methods are useful for creating reusable functions that perform tasks related to objects
let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		state: "Texas",
	},
	email: ["joe@gmail.com", "joesmith@email.xyz"],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + " " + this.lastName);
	}
};

friend.introduce();

// [SECTION] Real World application of objects
/*
	- Scenario
	1. We would like to create a game that would have several pokemon interact with each other
	2. Every pokemon would have the same set of stat,s
	 properties and functions
*/

// Using object literals to create multiple kinds of pokemon would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attach: 50,
	tackle: function(){
		console.log("This pokemon talked targetPokemon");
		console.log("targetPokemon's health is now reducted to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor instead will help with the process
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + " fainted");
	}
};

// Create new instances of the new "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);