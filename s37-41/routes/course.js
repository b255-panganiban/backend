const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
// S39 activity
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		courseController.addCourse(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});


// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController)
		)
});

// Route for retrieving all ACTIVE  courses
// Middleware for verifying JWT is not required because users who are not logged in should also be able to view the courses

router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController)
		)
});

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending upon the information provided
router.get("/:courseID", (req, res) => {
	console.log(req.params.courseID);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all parameters provided via the URL
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


// Route for updating a course
// JWT Verfication is needed for this route to ensure that a user is logged in before updating a course
router.put("/:courseID", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
});


// Route for archiving Course
router.put("/:courseID/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
		} else {
			res.send(false);
		}
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;