//  1. Show only the firstName and lastName fields and hide the _id field

db.users.find({ $or: [
	{firstName: {$regex: "s", $options: "i"}}, 
	{lastName: {$regex: "d", $options: "i"}}
	]},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);

// 2. Find users who have none in the company property and their age is greater than or equal to 70.

db.users.find({ $and: [
	{company : "none"}, 
	{age : {$gte: 70}}
	]}
);


// 3. Find users with the letter e in their first name and has an age of less than or equal to 30.

db.users.find({ $and: [
	{firstName: {$regex: "e", $options: "i"}}, 
	{age : {$lte: 30}}
	]}
);