console.log("Hello World");

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

			// If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the counter number  value is divisible by 5, print/console log the counter number.

	function printNumbers(){
		let num = prompt("Enter a number:");
		console.log(`Number provided is ${num}`);

		for(num; num >= 0; num--){
			 if (num <= 50){
				console.log("The current value is at " + num + ". Terminating the loop.")
				break;
			} else if (num % 10 === 0){
				console.log("The number is divisible by 10. Skipping the number.");
				continue;
			} else if (num % 5 === 0){
				console.log(num)
			}
		}
	}
	printNumbers();




//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for(let i = 0; i < string.length; i++){

	if(
		string[i].toLowerCase() == "a" ||
		string[i].toLowerCase() == "e" ||
		string[i].toLowerCase() == "i" ||
		string[i].toLowerCase() == "o" ||
		string[i].toLowerCase() == "u"
		) {
		continue;
		} else 
		filteredString += string[i];
		}
console.log(filteredString);


//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}