console.log("Hello World!");

console.log(fetch('https://jsonplaceholder.typicode.com/todos'));
fetch('https://jsonplaceholder.typicode.com/todos').then(response => console.log(response.status));

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos');
	let json = await result.json();
}
fetchData();

// GET
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map(json => json.title)));


// GET single todo list
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

// POST method
fetch('https://jsonplaceholder.typicode.com/todos/',{
	method: 'POST',
	headers: {
		'Content-type': 'application/json', 
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
	.then((response) => response.json())
	.then((json) => console.log(json));

// PUT method
	fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PUT',

		headers: {
			'Content-type': 'application/json', 
		},
		body: JSON.stringify({
			dateCompleted: 'Pending',
			description: 'To update the my to do list with a different data structure',
			status: 'Pending',
			title: 'Updated To Do List Item',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete",
		title: 'deletctus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// DELETE method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})