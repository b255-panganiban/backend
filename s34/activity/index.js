const express = require("express")
const app = express()
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// GET Method [Home]
app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
});

let users = [{
	"username": "johndoe",
	"password": "johndoe1234"
}];

// GET Method [Users]
app.get("/users", (req, res) => {
	res.send(users)
});

// DELETE Method [Delete-user]
app.delete("/delete-user", (req, res) => {
	let message;
	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users.splice([i]);

			message = `User ${req.body.username} has been deleted. `;

			break;
		} else {
			message = `No users found.`;
		}
	} if(message == ''){
		message = `User does not exist.`;
	}
	res.send(message);
	console.log(users)
})


// LISTEN
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;